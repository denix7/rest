package com.example.rest.controllers;

import com.example.rest.api.SystemPropertyController;
import com.example.rest.exceptions.SystemPropertyNotFoundException;
import com.example.rest.model.SystemProperty;
import com.example.rest.service.SystemPropertyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(SystemPropertyController.class)
public class SystemPropertyControllerTest {

    @MockBean
    SystemPropertyService systemPropertyService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    String baseUrl = "/api/v1/system-property";

    @Test
    void whenGetSystemPropertyBaseUrl_thenReturns200() throws Exception {
        mockMvc.perform(get(baseUrl)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void whenCreateSystemProperty_thenReturns200() throws Exception {
        SystemProperty systemProperty = new SystemProperty();
        systemProperty.setName("java.awt.headless");

        mockMvc.perform( MockMvcRequestBuilders
                .post(baseUrl + "/register")
                .content(asJsonString(systemProperty))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void whenGetSystemPropertyIsTrue_thenReturns200() throws Exception {
        UUID id = UUID.randomUUID();
        String correctSystemPropertyName = "java.awt.headless";

        SystemProperty systemPropertyRequest = new SystemProperty();
        systemPropertyRequest.setName(correctSystemPropertyName);

        SystemProperty systemPropertyResponse = new SystemProperty(id, correctSystemPropertyName, "true");

        Mockito.when(systemPropertyService.getSystemProperty(systemPropertyRequest))
                .thenReturn(systemPropertyResponse);

        mockMvc.perform(MockMvcRequestBuilders
                .post( "/get-property")
                .content(mapper.writeValueAsString(systemPropertyRequest))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenGetSystemPropertyIsFalse_thenReturns404() throws Exception {
        UUID id = UUID.randomUUID();
        String wrongSystemPropertyName = "java.awt.wrong-system-property";

        SystemProperty systemProperty = new SystemProperty();
        systemProperty.setName(wrongSystemPropertyName);

        SystemPropertyNotFoundException exception = new SystemPropertyNotFoundException(wrongSystemPropertyName);

        Mockito.when(systemPropertyService.getSystemProperty(systemProperty))
                .thenThrow(exception);

        mockMvc.perform(MockMvcRequestBuilders
                .post( "/get-property")
                .content(mapper.writeValueAsString(systemProperty))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
