package com.example.rest.services;

import com.example.rest.dao.SystemPropertyDao;
import com.example.rest.model.SystemProperty;
import com.example.rest.service.SystemPropertyService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class SystemPropertyServiceTest {

    @MockBean
    private SystemPropertyService systemPropertyService;

    @Mock
    SystemPropertyDao systemPropertyDao;

    @Test
    public void testGetSystemPropertyTrue() {
        UUID id = UUID.randomUUID();
        SystemProperty systemPropertyRequest = new SystemProperty();
        systemPropertyRequest.setName("java.awt.headless");

        SystemProperty systemPropertyResponse = new SystemProperty(id, "java.awt.headless", "true");

        Mockito.when(systemPropertyDao.getSystemProperty(systemPropertyRequest))
                .thenReturn(Optional.of(systemPropertyResponse));

        Optional<SystemProperty> result = systemPropertyDao.getSystemProperty(systemPropertyRequest);

        assertEquals(systemPropertyResponse, result.get());
    }

    @Test
    public void testGetSystemPropertyFalse() {
        UUID id = UUID.randomUUID();
        SystemProperty systemPropertyRequest = new SystemProperty();
        systemPropertyRequest.setName("java.awt.wrong-system-property");

        SystemProperty systemPropertyResponse = new SystemProperty(id, "java.awt.headless", "true");

        Mockito.when(systemPropertyDao.getSystemProperty(systemPropertyRequest))
                .thenReturn(Optional.ofNullable(null));

        Optional<SystemProperty> result = systemPropertyDao.getSystemProperty(systemPropertyRequest);

        assertEquals(Optional.empty(), result);
    }

    @Test
    public void testWhenGetAllThenShouldReturnListOfSystemProperties() {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();

        SystemProperty sp1 = new SystemProperty(id1, "java.runtime.name", "Java(TM) SE Runtime Environment");
        SystemProperty sp2 = new SystemProperty(id2, "spring.beaninfo.ignore", "true");
        SystemProperty sp3 = new SystemProperty(id3, "java.runtime.name", "25.281-b09");

        List<SystemProperty> list = Arrays.asList(sp1, sp2, sp3);

        Mockito.when(systemPropertyDao.getAll())
                .thenReturn(list);

        Collection<SystemProperty> result = systemPropertyDao.getAll();

        assertEquals(3, result.size());
    }
}
