package com.example.rest.service;

import com.example.rest.model.SystemProperty;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SystemPropertyService {

    public int insertSystemProperty(SystemProperty systemProperty);

    public SystemProperty getSystemProperty(SystemProperty systemProperty);

    public List<SystemProperty> getAll();

    public Optional<SystemProperty> findById(UUID id);

}
