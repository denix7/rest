package com.example.rest.service;

import com.example.rest.dao.SystemPropertyDao;
import com.example.rest.exceptions.SystemPropertyNotFoundException;
import com.example.rest.model.SystemProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SystemPropertyServiceImpl implements SystemPropertyService {

    private final SystemPropertyDao systemPropertyDao;

    @Autowired
    public SystemPropertyServiceImpl(@Qualifier("fakeSystemPropertyDao") SystemPropertyDao systemPropertyDao) {
        this.systemPropertyDao = systemPropertyDao;
    }

    public int insertSystemProperty(SystemProperty systemProperty) {
        return systemPropertyDao.insertPropertyService(systemProperty);
    }

    public SystemProperty getSystemProperty(SystemProperty systemProperty) {
        Optional<SystemProperty> element = systemPropertyDao.getSystemProperty(systemProperty);
        if(element.isPresent()) {
            return element.get();
        }
        throw new SystemPropertyNotFoundException(systemProperty.getName());
    }

    public List<SystemProperty> getAll() {
        return systemPropertyDao.getAll();
    }

    public Optional<SystemProperty> findById(UUID id) {
        return systemPropertyDao.findPropertyServiceById(id);
    }
}
