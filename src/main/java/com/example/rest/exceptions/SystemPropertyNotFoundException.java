package com.example.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SystemPropertyNotFoundException extends RuntimeException {
    public SystemPropertyNotFoundException(String name) {
        super("name" + name);
    }
}
