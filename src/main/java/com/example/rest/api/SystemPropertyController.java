package com.example.rest.api;

import com.example.rest.exceptions.SystemPropertyNotFoundException;
import com.example.rest.model.SystemProperty;
import com.example.rest.service.SystemPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

@RequestMapping("api/v1/system-property")
@RestController
public class SystemPropertyController {

    @Autowired
    private final SystemPropertyService systemPropertyService;

    public SystemPropertyController(SystemPropertyService SystemPropertyService) {
        this.systemPropertyService = SystemPropertyService;
    }

    @PostMapping("/register")
    public void addSystemProperty(@RequestBody SystemProperty systemProperty) {
        systemPropertyService.insertSystemProperty(systemProperty);
    }

    @PostMapping("/get-property")
    public ResponseEntity<SystemProperty> getSystemPropertyTrue(@RequestBody SystemProperty systemProperty) {
        systemPropertyService.getAll();
        return ResponseEntity.ok(systemPropertyService.getSystemProperty(systemProperty));
    }

    @GetMapping()
    public List<SystemProperty> getAll() {
        return systemPropertyService.getAll();
    }

    @GetMapping(path = "{id}")
    public SystemProperty getById(@PathVariable("id") UUID id) {
        return systemPropertyService.findById(id).orElse(null);
    }
}
