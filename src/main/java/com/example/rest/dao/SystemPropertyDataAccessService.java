package com.example.rest.dao;

import com.example.rest.model.SystemProperty;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("fakeSystemPropertyDao")
public class SystemPropertyDataAccessService implements SystemPropertyDao {

    private static List<SystemProperty> DB = new ArrayList<>();

    @Override
    public int insertPropertyService(UUID id, SystemProperty systemProperty) {
        DB.add(new SystemProperty(id, systemProperty.getName(), systemProperty.getValue()));
        return 0;
    }

    @Override
    public Optional<SystemProperty> getSystemProperty(SystemProperty systemProperty) {
            for (SystemProperty current: DB) {
                if(current.getName().equals(systemProperty.getName())) {
                    if(current.getValue().equals("true")) {
                        return Optional.of(current);
                    }
                }
            }
            return Optional.ofNullable(null);
    }

    @Override
    public Optional<SystemProperty> findPropertyServiceById(UUID id) {
        return DB.stream().filter(systemProperty -> systemProperty.getId().equals(id)).findFirst();
    }

    @Override
    public List<SystemProperty> getAll() {
        Properties p = new Properties(System.getProperties());
        Set<String> keys = p.stringPropertyNames();

        for (String key : keys) {
            DB.add(new SystemProperty(UUID.randomUUID(), key, p.getProperty(key)));
        }

        return DB;
    }
}
