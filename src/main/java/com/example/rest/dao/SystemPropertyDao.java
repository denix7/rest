package com.example.rest.dao;

import com.example.rest.model.SystemProperty;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SystemPropertyDao {

    int insertPropertyService(UUID id, SystemProperty systemProperty);

    default int insertPropertyService(SystemProperty systemProperty) {
        UUID id = UUID.randomUUID();
        return insertPropertyService(id, systemProperty);
    }

    Optional<SystemProperty> getSystemProperty(SystemProperty systemProperty);

    Optional<SystemProperty> findPropertyServiceById(UUID id);

    List<SystemProperty> getAll();
}
