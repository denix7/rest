package com.example.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class SystemProperty {
    private  String name;
    private  String value;
    private  UUID id;

    public SystemProperty() { }

    public SystemProperty(@JsonProperty("id") UUID id, @JsonProperty("name") String name, @JsonProperty("value") String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
